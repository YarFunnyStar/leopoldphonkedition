﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItsATrap : MonoBehaviour
{
    public float time;
    public float AttackRange;
    public LayerMask EnemyLayer;
    public int Damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            Collider2D[] EnimiesToDamage = Physics2D.OverlapCircleAll(transform.position, AttackRange, EnemyLayer);
            foreach (Collider2D col in EnimiesToDamage)
            {
                col.GetComponent<Enemy>().TakeDamage(Damage);
            }
            Destroy(gameObject);
        }
    }



    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackRange);
    }
}
