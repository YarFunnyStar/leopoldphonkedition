﻿using UnityEngine;

public class FallingObject : MonoBehaviour
{
    private bool hasFelt = false;
    public Animator animator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BrownCat"))
        {
            animator.SetBool("StartFalling", true);

        }
        if (collision.CompareTag("WhiteCat"))
        {
            if (hasFelt)
            {
                animator.SetBool("IdleStart", true);
                animator.SetBool("StartFalling", false);
                hasFelt = false;
            }


        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("WhiteCat"))
        {
            if (hasFelt)
            {
                animator.SetBool("IdleStart", true);
                animator.SetBool("StartFalling", false);
                hasFelt = false;
            }

        }
    }

    public void HasFelt()
    {
        hasFelt = true;
        Debug.Log("Felt!");
    }

    public void HasNotFelt()
    {
        hasFelt = false;
    }


}
