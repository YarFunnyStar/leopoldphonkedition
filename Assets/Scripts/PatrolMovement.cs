﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolMovement : MonoBehaviour
{

    public Transform[] points;
    public float speed;
    private Rigidbody2D rb;
    public float stopDistance;
    private int i = 0;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();

    }


    void Update()
    {
        GoToPoint(points[i]);

        if (Vector2.Distance(points[i].position, transform.position) < stopDistance)
        {

            i = Random.Range(0, points.Length);
        }
    }

    public void GoToPoint(Transform point)
    {

        transform.LookAt2D(point);
        rb.velocity = transform.up * speed;
    }


}
static class Extensions
{
    #region LookAt2D
    public static void LookAt2D(this Transform me, Vector3 target, Vector3? eye = null)
    {
        float signedAngle = Vector2.SignedAngle(eye ?? me.up, target - me.position);

        if (Mathf.Abs(signedAngle) >= 1e-3f)
        {
            var angles = me.eulerAngles;
            angles.z += signedAngle;
            me.eulerAngles = angles;
        }
    }
    public static void LookAt2D(this Transform me, Transform target, Vector3? eye = null)
    {
        me.LookAt2D(target.position, eye);
    }
    public static void LookAt2D(this Transform me, GameObject target, Vector3? eye = null)
    {
        me.LookAt2D(target.transform.position, eye);
    }

    public static void LookAt2DInverse(this Transform me, Vector3 target, Vector3? eye = null)
    {
        float signedAngle = Vector2.SignedAngle(eye ?? me.up, target - me.position);

        if (Mathf.Abs(signedAngle) >= 1e-3f)
        {
            var angles = -me.eulerAngles;
            angles.z -= signedAngle;
            me.eulerAngles = angles;
        }
    }
    public static void LookAt2DInverse(this Transform me, Transform target, Vector3? eye = null)
    {
        me.LookAt2DInverse(target.position, eye);
    }
    public static void LookAt2DInverse(this Transform me, GameObject target, Vector3? eye = null)
    {
        me.LookAt2DInverse(target.transform.position, eye);
    }

    #endregion
}
