﻿using UnityEngine;
using UnityEngine.UI;

public class Setting : MonoBehaviour
{
    public GameObject setting;
    public bool openSet = false;
    public bool menu = false;

    public Toggle toggle;
    public Slider slider;

    private float vol;
    private bool full;

    public void Options()
    {
        if (openSet)
        {
            openSet = false;
        }
        else
        {
            openSet = true;
        }
    }

    public void ChangeVolume(float val)
    {
        vol = val;
    }

    public void ChangeFullscreen(bool val)
    {
        full = val;
    }

    public void Exit()
    {
        setting.SetActive(false);
        openSet = false;
    }

    public void SaveSetting()
    {
        if (full)
        {
            PlayerPrefs.SetInt("fullscreen", 0);
            Screen.fullScreen = true;
            Screen.SetResolution(1920, 1080, true);
        }
        else
        {
            PlayerPrefs.SetInt("fullscreen", 1);
            Screen.fullScreen = false;
            Screen.SetResolution(1920, 1080, false);
        }
        PlayerPrefs.SetFloat("volume", vol);

        setting.SetActive(false);
        openSet = false;
    }


    void Awake()
    {
        if (PlayerPrefs.GetInt("fullscreen") == 0)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }
        slider.value = PlayerPrefs.GetFloat("volume");

        setting.SetActive(false);
        openSet = false;
    }

    void Update()
    {
        if (menu)
        {
            if (openSet)
            {
                setting.SetActive(false);
            }
            else
            {
                Open();
            }
        }
        else
        {
            if (openSet)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    setting.SetActive(false);
                    openSet = false;
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    Open();
                    openSet = true;
                }
            }
        }
    }

    public void Open()
    {
        if (PlayerPrefs.GetInt("fullscreen") == 0)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }
        slider.value = PlayerPrefs.GetFloat("volume");
        setting.SetActive(true);
    }
}