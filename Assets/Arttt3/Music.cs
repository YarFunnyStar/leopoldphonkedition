﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public AudioSource pleer;
    public AudioClip[] treks;

    int currentTrek = 0;
    int numberTrek;

    private bool play = true;

    void Awake()
    {
        numberTrek = treks.Length - 1;
        SelectTrek(currentTrek);
        pleer.Play();
    }

    public void Pause()
    {
        if (play)
        {
            pleer.Play();
            play = false;
        }
        else
        {
            pleer.Stop();
            play = true;
        }
    }

    public void Past()
    {
        if (currentTrek + 1 <= numberTrek)
        {
            currentTrek++;
            SelectTrek(currentTrek);
            pleer.Play();
        }
        else
        {
            currentTrek = 0;
            SelectTrek(currentTrek);
        }
    }
    
    public void Before()
    {
        if (currentTrek - 1 >= 0)
        {
            currentTrek--;
            SelectTrek(currentTrek);
            pleer.Play();
        }
        else
        {
            currentTrek = numberTrek;
            SelectTrek(currentTrek);
        }
    }

    void Update()
    {
        pleer.volume = PlayerPrefs.GetFloat("volume");

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (play)
            {
                pleer.Play();
                play = false;
            }
            else
            {
                pleer.Stop();
                play = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Equals))
        {
            if (currentTrek + 1 <= numberTrek)
            {
                currentTrek++;
                SelectTrek(currentTrek);
                pleer.Play();
            }
            else
            {
                currentTrek = 0;
                SelectTrek(currentTrek);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Minus)) 
        {
            if (currentTrek - 1 >= 0)
            {
                currentTrek--;
                SelectTrek(currentTrek);
                pleer.Play();
            }
            else
            {
                currentTrek = numberTrek;
                SelectTrek(currentTrek);
            }
        }

    }

    void SelectTrek(int i)
    {
        for (int cnt = 0; cnt < treks.Length; cnt++)
        {
            if (cnt == i)
            {
                pleer.clip = treks[cnt];
            }
        }
    }
}