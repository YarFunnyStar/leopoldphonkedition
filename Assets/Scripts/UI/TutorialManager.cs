﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public GameObject[] popUps;
    private int popUpIndex;
    public GameObject mob;

   

    void Update()
    {

        for (int i = 0; i < popUps.Length; i++)
        {
            if (i == popUpIndex)
            {
                popUps[i].SetActive(true);
            }
            else
            {
                popUps[i].SetActive(false);
            }
        }


        if (popUpIndex == 0)
        {
            if (Input.GetKeyDown(KeyCode.D) || (Input.GetKeyDown(KeyCode.S)))
            {
                popUpIndex++;
            }
        }
        else if (popUpIndex == 1)
        {
            if (Input.GetMouseButton(0))
            {
                popUpIndex++;
            }
        }
        else if (popUpIndex == 2)
        {
            mob.SetActive(true);
            
        }
       
    }
}
