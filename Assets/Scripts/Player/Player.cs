﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private float MoveInputHorizontal;
    private float MoveInputVertical;
    public float speed;

    public GameObject DeathEffect;
  
    private Rigidbody2D rb;
    public Animator anim;
    public GameObject Trap;
    public int LimitTrap;

    

    




    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        Movement();

        if (LimitTrap >= 0)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Instantiate(Trap, transform.position, Quaternion.identity);
                LimitTrap--;
            }
            
        }


    }


    public void Movement()
    {
        //MOVEMENT
        MoveInputHorizontal = Input.GetAxisRaw("Horizontal");
        MoveInputVertical = Input.GetAxisRaw("Vertical");
        rb.velocity = new Vector2(MoveInputHorizontal * speed, MoveInputVertical * speed);
        //MOVEMENT



        //ANIMATIONS
        anim.SetFloat("Horizontal", MoveInputHorizontal);
        anim.SetFloat("Vertical", MoveInputVertical);


        if (MoveInputHorizontal != 0 || MoveInputVertical != 0)
        {
            anim.SetBool("IsMoving", true);
        }
        else { anim.SetBool("IsMoving", false); }
        //ANIMATIONS
    }


}























