﻿using UnityEngine;

public class Cat : MonoBehaviour
{
    public Controller controller;
    public float power = 0.25f;
    
    private Rigidbody2D rgb;

    void Awake()
    {
        rgb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && rgb.velocity.y <= 5 && controller.clic && Time.timeScale == 1)
        {
            rgb.AddForce(new Vector2(0, power), ForceMode2D.Impulse);
        }
    }
}
