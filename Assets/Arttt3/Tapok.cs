﻿using UnityEngine;

public class Tapok : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Cat")
        {
            Destroy(collision.gameObject);
        }
    }
}
