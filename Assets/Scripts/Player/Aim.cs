﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
  
    public float FireRate;

    public Animator anim;

    //ataka
    public float TimeBetweenAttacks;
    float NextAttackTime;
    public Transform AttackPoint;
    public float AttackRange;
    public LayerMask EnemyLayer;
    public int Damage;
    //ataka

    void Update()
    {
        WeaponRotation();

        //Attack
        if (Time.time > NextAttackTime)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                NextAttackTime = Time.time + TimeBetweenAttacks;

                Attack();


            }
        }
        //Attack
      
    }
    void WeaponRotation() // vot eto vot povorachivaet nashego persa sled za mishkoi, kak eto rabotaet - naychnaya fantastika
    {
        Vector2 dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 10 * Time.deltaTime);
    }

    
    public void Attack()
    {
        Collider2D[] EnimiesToDamage = Physics2D.OverlapCircleAll(AttackPoint.position, AttackRange, EnemyLayer);
        foreach (Collider2D col in EnimiesToDamage)
        {
            col.GetComponent<Enemy>().TakeDamage(Damage);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(AttackPoint.position, AttackRange);
    }


}
