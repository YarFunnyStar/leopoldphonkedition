﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rats : MonoBehaviour
{
    public int damage;
    public int health;

    public GameObject blood;
    public GameObject deathEffect;


    //public GameObject HealthBar;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PointMove")
        {

            collision.GetComponent<Cheese>().TakeDamage(damage);
        }


    }

    public void TakeDamage(int damage)
    {

        health -= damage;
        if (health <= 0)
        {
            Instantiate(deathEffect, transform.position, Quaternion.identity);
            Instantiate(blood, transform.position, Quaternion.identity);
            Destroy(gameObject);
           
        }
       
    }
}
