﻿using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public Text startTimer;
    public Text endTimer;

    public Rigidbody2D cat;
    public GameObject tapok;
    public float maxSpeed = 5;
    public float y = 1.5f;
    public float startTime = 5;
    public float gameTimer = 100;
    public bool clic = false;

    private Vector3 target;
    private float timeAntiSpeed = 0;
    private float antiSpeed = 0;
    private float speed = 0;
    private bool oneEnd = false;

    void FixedUpdate()
    {
        if (startTime > 0)
        {
            startTimer.text = "" + ((int)startTime + 1);
            startTime -= Time.deltaTime;
        }
        else if (cat != null && gameTimer > 0 && Time.timeScale == 1)
        {
            startTimer.gameObject.SetActive(false);
            clic = true;

            /*
            if (Input.GetMouseButtonDown(0) && cat.velocity.y <= 5)
            {
                cat.AddForce(new Vector2(0, 0.1f), ForceMode2D.Impulse);
            }
            */
        
            if (cat.velocity.y <= 2)
            {
                antiSpeed = 0;
            }
            else if (cat.velocity.y > 2 && cat.velocity.y <= 3)
            {
                antiSpeed = 0.075f;
            }
            else if (cat.velocity.y > 3 && cat.velocity.y <= 4)
            {
                antiSpeed = 0.1f;
            }
            else if (cat.velocity.y > 4 && cat.velocity.y <= 5)
            {
                antiSpeed = 0.15f;
            }

            target = new Vector3(cat.transform.position.x, cat.transform.position.y + y - cat.velocity.y, cat.transform.position.z);
            tapok.transform.position = Vector2.Lerp(tapok.transform.position, target, speed * Time.deltaTime);

            if (timeAntiSpeed > 0)
            {
                timeAntiSpeed -= Time.deltaTime;
            }
            else
            {
                timeAntiSpeed = 0.1f;
                cat.AddForce(new Vector2(0, -antiSpeed), ForceMode2D.Impulse);

                if (speed < maxSpeed) 
                {
                    speed += 0.03f;
                }
            }

            if (gameTimer > 0)
            {
                gameTimer -= Time.deltaTime;
            }

            endTimer.text = "" + ((int)gameTimer + 1);
        }
        else if (!oneEnd)
        {
            endTimer.gameObject.SetActive(false);

            if (cat == null)
            {
                Debug.Log("Defeat");
            }
            else
            {
                cat.AddForce(-cat.velocity, ForceMode2D.Impulse);
                clic = false;
                Debug.Log("Win");
            }
            oneEnd = true;
        }
    }
}