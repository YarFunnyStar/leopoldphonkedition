﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMove : MonoBehaviour
{
    public LayerMask player;
    public LayerMask Trap;
    string playerTag = "Player";

    public bool CheckHowMove;
    public float SpeedMove;
    public float RelaxDistance;

    public float runTime = 0.3f;


    private bool CheckPoint;
    private int CheckElement = 0;
    private List<GameObject> waypointsList;//массив точек в игре
    private int waypointsCount = 0; //счетчик точек в игре
    private GameObject Target; //текущая цель
    public Transform TargetMove;
    public Transform Cat;

    public float bulletForce = 10f;
    public float radius;
  //  public Transform PointGun;
   // public GameObject bulletPrefab;
    public float dalay;
    private float time;
    private float timeRunAtPlayer;
    void Start()
    {
        

    }
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.tag == "Player")
    //    {

    //        Debug.Log("lol");
    //    }
    //}

    void Update()
    {
        Collider2D collider = Physics2D.OverlapCircle(transform.position, radius, player);

        if (collider == true)
        {
            timeRunAtPlayer = runTime;
        }

        if (timeRunAtPlayer > 0)
        {
            MoveOnPlayer();
            timeRunAtPlayer -= Time.deltaTime;
            
        }
        else
        {
            Collider2D _collider = Physics2D.OverlapCircle(transform.position, radius, Trap);
            if(_collider == true)
            {
                MoveOnTrap();
            }
            else
            {
                MoveOnPoint();
            }
            
        }
    }
    public void MoveOnTrap()
    {
        GameObject Trap = GameObject.FindGameObjectWithTag("Trap");
        var dir = Trap.transform.position - transform.position;
        if (dir.sqrMagnitude > RelaxDistance * RelaxDistance)
        {
            float step = SpeedMove * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, Trap.transform.position, step);

            transform.LookAt2D(Trap);



        }

    }

    public void MoveOnPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag(playerTag);
        var dir = player.transform.position - transform.position;
        if (dir.sqrMagnitude > RelaxDistance * RelaxDistance)
        {
            float step = SpeedMove * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, -step);
            
            transform.LookAt2D(player);
            
            

        }
    }

    public void MoveOnPoint()
    {
        waypointsList = new List<GameObject>(GameObject.FindGameObjectsWithTag("PointMove"));
        waypointsCount = waypointsList.Count; //количество точек в массиве


        if (Target != null && CheckPoint == true) //если у нас есть цель
        {
            float step = SpeedMove * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, step);
            transform.LookAt2D(TargetMove);

            if (Target.transform.position - transform.position == new Vector3(0, 0, 0))
            {
                CheckPoint = false;
                CheckElement = Random.Range(0, waypointsCount);

            }
        }
        else //если нету цели
        {

            Target = waypointsList[CheckElement];
            CheckPoint = true;


        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "PointMove" || collision.tag == "Destroy")
        {
            Destroy(gameObject);
        }
        
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);

    }
}
